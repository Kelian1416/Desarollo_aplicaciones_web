***programacion***
5AVP
Kelian Alejandro Cota Ibarra

-Practica #1 - 02/09/2022 - Practica de ejemplo
Commit: a83d397ecb697da9639afc4793736b1885111806
Archivo: https://gitlab.com/Kelian1416/Desarollo_aplicaciones_web/-/blob/main/Parcial1/practica_ejemplo.html

-Practica #2 - 09/09/2022 - Practica JavaScript
Commit: 4c45c49dedd9ff36d14e33b76034b70ac7931682
Archivo: https://gitlab.com/Kelian1416/Desarollo_aplicaciones_web/-/blob/main/Parcial1/Practica2-JavaScript.html

-Practica #3 - 15/09/2022 - Practica web con bases de datos-parte 1
Commit: 1bb1f677ee5f28f511002823c6fa0fdd89755be5
Archivo: https://gitlab.com/Kelian1416/Desarollo_aplicaciones_web/-/blob/main/Parcial1/PracticaWebDatos.zip

-Practica #4 - 19/09/2022 - Practica web con bases de datos - Vista de consulta de datos
Commit: cfa807b51bb5941d2e7bf2fd4e51b5cdcae5573d
Archivo: https://gitlab.com/Kelian1416/Desarollo_aplicaciones_web/-/blob/main/Parcial1/PracticaWebDatos/ConsultarDatos.php

-Practica #5 - 19/09/2022 - Practica web con bases de datos - Vista de registro de datos
Commit: 87bcabcf2816038118da612e2ca1d68ca40c6fdf
Archivo: https://gitlab.com/Kelian1416/Desarollo_aplicaciones_web/-/blob/main/Parcial1/PracticaWebDatos/registrarDatos.html
